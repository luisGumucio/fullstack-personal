package com.fullstack.full.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Data
@Table(name = "persona_juridica")
public class PersonaJuridica {
    @Id
    @GeneratedValue
    private long id;
    private String razonSocial;
    private String nit;
    private String direccion;
    private String telefono;
    @OneToOne
    @JoinColumn(name = "id_usuario_financiero")
    private UsuarioFinanciero usuarioFinanciero;
}
