package com.fullstack.full.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "persona_natural")
public class PersonaNatural {
    @Id
    @GeneratedValue
    private long id;
    private String nombres;
    private String primerApellido;
    private String segundoApellido;
    private String nroDocumentoIdentidad;
    private String celular;
    private String direccionDomicilio;
    @OneToOne
    @JoinColumn(name = "id_usuario_financiero")
    private UsuarioFinanciero usuarioFinanciero;
}
