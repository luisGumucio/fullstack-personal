package com.fullstack.full.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@Table(name = "usuario_financiero")
@Inheritance(strategy = InheritanceType.JOINED)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioFinanciero {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "fecha_creacion")
    private Date fechaCreacion;
    @Column(name ="usuario_creacion")
    private String usuarioCreacion;
    @Column(name = "fecha_modificacion")
    private Date fechaModificacion;
    @Column(name = "usuario_modificacion")
    private String usuarioModificacion;

}
