package com.fullstack.full.dtoes;

import lombok.Data;

@Data
public class PersonaDto {
    private String usuarioCreacion;
    private String razonSocial;
    private String nit;
    private String direccion;
    private String telefono;
}
