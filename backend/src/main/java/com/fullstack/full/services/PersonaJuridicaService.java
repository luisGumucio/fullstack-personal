package com.fullstack.full.services;

import com.fullstack.full.dtoes.PersonaDto;
import com.fullstack.full.models.PersonaJuridica;
import com.fullstack.full.models.UsuarioFinanciero;
import com.fullstack.full.repositories.PersonaJuridicaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PersonaJuridicaService {
    private final PersonaJuridicaRepository personaJuridicaRepository;
    private final UsuarioFinancieroService usuarioFinancieroService;
    public PersonaJuridicaService(PersonaJuridicaRepository personaJuridicaRepository,
                                  UsuarioFinancieroService usuarioFinancieroService) {
        this.personaJuridicaRepository = personaJuridicaRepository;
        this.usuarioFinancieroService = usuarioFinancieroService;
    }

    public void savePersonJuridica(PersonaDto personaDto) {
        UsuarioFinanciero usuarioFinanciero = usuarioFinancieroService.saveUsuario(personaDto);
        PersonaJuridica personaJuridica = new PersonaJuridica();
        personaJuridica.setNit(personaDto.getNit());
        personaJuridica.setDireccion(personaDto.getDireccion());
        personaJuridica.setTelefono(personaDto.getTelefono());
        personaJuridica.setRazonSocial(personaDto.getRazonSocial());
        personaJuridica.setUsuarioFinanciero(usuarioFinanciero);
        personaJuridicaRepository.save(personaJuridica);
    }

    public Page<PersonaJuridica> getAllPerson(String nit, String razonSocial, Pageable pageable) {
        return personaJuridicaRepository.findAll(nit, razonSocial, pageable);
    }

    public void deletePerson(long id) {
        personaJuridicaRepository.deleteById(id);
    }
}
