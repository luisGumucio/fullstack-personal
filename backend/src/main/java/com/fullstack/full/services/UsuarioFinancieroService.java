package com.fullstack.full.services;

import com.fullstack.full.dtoes.PersonaDto;
import com.fullstack.full.models.UsuarioFinanciero;
import com.fullstack.full.repositories.UsuarioFinancieroRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UsuarioFinancieroService {
    private final UsuarioFinancieroRepository usuarioFinancieroRepository;
    public UsuarioFinancieroService(UsuarioFinancieroRepository usuarioFinancieroRepository) {
        this.usuarioFinancieroRepository = usuarioFinancieroRepository;
    }

    public UsuarioFinanciero saveUsuario(PersonaDto personaDto) {
        UsuarioFinanciero usuarioFinanciero = UsuarioFinanciero.builder()
                .usuarioCreacion(personaDto.getUsuarioCreacion())
                .fechaCreacion(new Date())
                .build();
        usuarioFinancieroRepository.save(usuarioFinanciero);
        return usuarioFinanciero;
    }
}
