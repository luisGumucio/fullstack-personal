package com.fullstack.full.controllers;

import com.fullstack.full.dtoes.PersonaDto;
import com.fullstack.full.models.PersonaJuridica;
import com.fullstack.full.services.PersonaJuridicaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("persona-juridica")
public class PersonaJuridicaController {
    private final PersonaJuridicaService personaJuridicaService;

    public PersonaJuridicaController(PersonaJuridicaService personaJuridicaService) {
        this.personaJuridicaService = personaJuridicaService;
    }
    @GetMapping
    public Page<PersonaJuridica> personaJuridicas(@RequestParam(value = "nit", required = false) String nit,
                                                  @RequestParam(value = "razon-social", required = false)
                                                  String razonSocial,
                                                  @PageableDefault Pageable pageable) {
        return personaJuridicaService.getAllPerson(nit, razonSocial, pageable);
    }

    @PostMapping
    public void save(@RequestBody PersonaDto personaDto) {
        personaJuridicaService.savePersonJuridica(personaDto);
    }

    @DeleteMapping(value = "/{id}")
    public void deletePerson(@PathVariable long id) {
        personaJuridicaService.deletePerson(id);
    }
}
