package com.fullstack.full.repositories;

import com.fullstack.full.models.PersonaJuridica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaJuridicaRepository extends JpaRepository<PersonaJuridica, Long> {

    Page<PersonaJuridica> findAllByNitAndRazonSocial(String nit, String razonSocial, Pageable pageable);
    @Query("SELECT p FROM PersonaJuridica p WHERE " +
            "(:nit IS NULL OR p.nit = :nit%) AND " +
            "(:razonSocial IS NULL OR p.razonSocial = :razonSocial)")
    Page<PersonaJuridica> findAll(String nit, String razonSocial, Pageable pageable);

}
