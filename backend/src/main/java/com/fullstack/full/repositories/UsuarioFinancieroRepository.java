package com.fullstack.full.repositories;

import com.fullstack.full.models.UsuarioFinanciero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioFinancieroRepository extends JpaRepository<UsuarioFinanciero,Long> {
}
